# Challenge GD4H - BaDCoPS

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Elle organise un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées.

Liens : 
<a href="https://challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Site</a> / 
<a href="https://forum.challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Forum</a>
a

## BaDCoPS

De nombreuses substances contenues dans les produits phytopharmaceutiques (PPP) ont un impact démontré sur la santé et l’environnement.

En termes de données, la Base nationale de vente distributeurs (BNV-D) est centrale sur ce sujet mais ne renseigne pas la toxicité/écotoxicité, les usages, ou encore les fonctions des produits et des substances actives qui composent les PPP. 

Ces informations sont contenues dans d’autres bases telles qu’Agritox, E-phy (ANSES) et des bases de données européennes.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=13" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

>TODO / **Description Solution**

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

>TODO / **documentation d'utilisation de la solution**

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).